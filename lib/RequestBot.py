import requests
import os
import json
import time
from datetime import datetime
from pprint import pprint
from random import random

class RequestBot:
    def __init__(self, **kwargs):
        # the subs to ping
        # there are a good amount of these in the sidebar of r4r https://old.reddit.com/r/r4r/
        # subs that work the best will have folks who follow the r4r posting format in the title of <age> [<r4r>] <location> - <subject>
        # TODO: more than 30 will likely catch 429 errors for too many reqs (30 per second)
        self.subs = kwargs.get('subs', ('r4r', 'r4r30plus', 'ForeverAloneDating', 'dateademi', 'cuddlebuddies', 'dirtyr4r', 'SoCalR4R'))
        # substring traits that are interesting -- these are checked in the title
        self.traits = kwargs.get('traits', ('r4r', 'r4m', 'r4f', 'f4r', 'f4f', 'f4m', 'm4r', 'm4m', 'm4f'))
        # substrings to make a post a priority -- these are checked in the title and body, after traits have been matched
        # these are not case sensitive, be careful using acronyms (note the whitespace around ' LA ')
        # these can be locations or other keywords that could be mentioned
        self.priorities = kwargs.get('priorities', ( 'Cali', 'California', 'Socal', ' LA ', 'Los Angeles', 'West Coast', 'beard' ))
        # boundaries of ages interested in
        self.min_age = kwargs.get('min_age', 18)
        self.max_age = kwargs.get('max_age', 99)
        # defualt time to sleep in seconds after all requests have been completed
        self.sleep_time = kwargs.get('sleep_time', 30)
        # filename to save to
        self.file_name = kwargs.get('file_name', 'posts.json')
        # user agent -- possible todo make this varaidic
        self.headers = { 'User-agent': f'my bot 0.1 {str(datetime.now())} {random()}' }
        # reddit base url
        self.BASE_URL = 'https://www.reddit.com'
        # for getting requests, filtering posts and writing them
        self.posts = []
        self.priority_posts = []

    def request(self):
        # clear out posts
        self.posts = []
        self.priority_posts = []
        responses = []

        # make a request to each sub, appending response to self, stripping out the junk
        for sub in self.subs:
            response = requests.get(f'{self.BASE_URL}/r/{sub}/new.json?limit=100',
                                    headers=self.headers)
            # jsonify responses and pull out ineresting information
            response = response.json()
            responses.append(response['data']['children'])

        # for each response, pull out the posts and throw the data in the post array
        for response in responses:
            for post in response:
                self.posts.append(post['data'])

        return self

    def filter(self):
        # filter out traits and ages that are interesting, create a separate list from the interesting posts of those that match priorities
        def trait_filter(post):
            # flag for trait checks
            is_interesting = False
            for trait in self.traits:
                if trait in post['title'].lower():
                    is_interesting = True
                    break # no reason to keep going if this redditor is interesting

            return is_interesting

        def age_filter(post):
            # flag for age check
            is_interesting = False
            # check if there is an age included in the title, and compare it to a
            # range we are interested in. If the age cannot be determined, include
            # this redditor 
            try:
                age = int(post['title'][:2])
                if age >= self.min_age and age <= self.max_age:
                    is_interesting = True
            except ValueError as e:
                is_interesting = True # set them to interesting even though they clearly are confused about how to format their post

            return is_interesting

        def priority_filter(post):
            is_priority = False
            for priority in self.priorities:
                if priority.lower() in post['title'].lower() or priority.lower() in post['selftext']:
                    # print(priority, post['url'])
                    is_priority = True
                    break

            return is_priority

        self.posts = list(filter(trait_filter, self.posts))
        self.posts = list(filter(age_filter, self.posts))
        self.priority_posts = list(filter(priority_filter, self.posts))
        # sync so each list is unique
        self.posts = [post for post in self.posts if post not in self.priority_posts]

        return self

    def write(self):
        # open file
        posts_json = {}
        with open(f'{os.path.dirname(__file__)}/{self.file_name}', 'r') as file:
            posts_json = json.load(file)

        # only modify the file if it is not in use
        if posts_json['is_open']:
            print('json not writeable, closing...')
            return self

        # pull out the intersting data 
        map_post = lambda post : { 'title': post['title'], 'url': post['url'], 'subreddit': post['subreddit'] }
        new_posts = [map_post(post) for post in self.posts]
        new_priority_posts = [map_post(post) for post in self.priority_posts]

        # make sure no duplicates are added to json
        posts_json['posts'] += [new_post for new_post in new_posts
                                if new_post not in posts_json['posts']
                                and new_post not in posts_json['seen']]
        posts_json['priority_posts'] += [new_post for new_post in new_priority_posts
                                         if new_post not in posts_json['priority_posts']
                                         and new_post not in posts_json['seen']]

        # write file
        with open(f'{os.path.dirname(__file__)}/{self.file_name}', 'w') as file:
             json.dump(posts_json, file)

        return self

    def run(self):
        # runs all of the bot's processes
        self.request()
        self.filter()
        self.write()
        time.sleep(self.sleep_time)
        self.run()

