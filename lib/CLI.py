import subprocess
import os
import json
from time import sleep

class CLI:
    def __init__(self):
        # the system open url command
        self.OPEN_COMMAND = 'open'
        # file to open
        self.FILE_NAME = 'posts.json'
        # amount of posts to open before prompting for input
        self.OPEN_COUNT = 10

    def display_all(self):
        self.display_count()
        self.display_priority()
        self.display_non_priority()
        return self

    def display_count(self):
        posts_json = {}
        with open(f'{os.path.dirname(__file__)}/{self.FILE_NAME}', 'r') as file:
            posts_json = json.load(file)

        print(f'priority posts: {len(posts_json["priority_posts"])}')
        print(f'posts: {len(posts_json["posts"])}')
        print(f'seen: {len(posts_json["seen"])}')
        return self

    def display_priority(self):
        self.display("priority_posts")
        self.wait_for_input()
        return self

    def display_non_priority(self):
        self.display("posts")
        return self

    def display(self, key):
        try:
            posts_json = {}
            with open(f'{os.path.dirname(__file__)}/{self.FILE_NAME}', 'r') as file:
                posts_json = json.load(file)

            # mark file as being open
            posts_json['is_open'] = True

            with open(f'{os.path.dirname(__file__)}/{self.FILE_NAME}', 'w') as file:
                json.dump(posts_json, file)

            count = 0
            for i, post in enumerate(posts_json[key]):
                print(f'opening {i + 1} of {len(posts_json[key])}: {post["url"]}')
                subprocess.Popen(f'{self.OPEN_COMMAND} {post["url"]}', shell=True)
                count += 1
                if count == self.OPEN_COUNT:
                    count = 0
                    self.wait_for_input()

            # mark all as read
            posts_json['seen'] += posts_json[key]
            posts_json[key] = []

            with open(f'{os.path.dirname(__file__)}/{self.FILE_NAME}', 'w') as file:
                json.dump(posts_json, file)
        finally:
            # clean up on exit
            posts_json = {}
            with open(f'{os.path.dirname(__file__)}/{self.FILE_NAME}', 'r') as file:
                posts_json = json.load(file)

            posts_json['is_open'] = False

            with open(f'{os.path.dirname(__file__)}/{self.FILE_NAME}', 'w') as file:
                json.dump(posts_json, file)

        return self

    def reset(self):
        subprocess.Popen(f'sh reset.sh', shell=True)

    def wait_for_input(self, message='press any key to continue'):
        return input(message)

    def mark_seen(self):
        pass

    def mark_priority_seen(self):
        pass

    def mark_non_priority_seen(self):
        pass
