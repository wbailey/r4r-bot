# r4r bot 😍

this python3 bot scrapes the dating/personals subs on reddit and filters posts based on
age, gender and other supplied parameters. Because clearly love, like every other 
problem, can be solved with automation.

## installing and using

* create a virtual enviroment: `virtualenv venv`, activate it `. venv/bin/activate` (might be a different path on non *nix systems)
* install the required packages `pip3 install -r requirements.txt`
* edit the supplied params to he `RequestBot` class in the `main()` function in `main.py`
* run `python3 main.py run`, it will ping the reddit API every 30 secponds or so and update `posts.json` with posts that might interest you
* update the options in `lib/CLI.py` (for example a linux system may need to change the `OPEN_COMMAND` to `'xdg-open'` or a wsl system may need to change it to `'explorer.exe'`
* cli args:
    * `run` continuosly runs the bot
    * `all` opens all interesting posts in the browser, 10 at a time
    * `count` displays information on posts
    * `non_priority` opens posts that matched age and gender filters, but not keywords
    * `priority` opens posts that matches age, gender and keyword filters
    * `reset` rests the tracked posts in the json file

## Todos:

* [ ] space out reqs to prevent 429s regardless of reqs made
* [ ] reafactor preferences to be dealbreakers and like to haves
* [ ] include the preferences matched in posts, possible flagging posts that match multiple
