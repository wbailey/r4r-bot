import sys
from lib.RequestBot import RequestBot
from lib.CLI import CLI

def main():
    argv = sys.argv[1:]

    cli = CLI()
    req_bot = RequestBot(
        min_age = 27,
        max_age = 40,
        traits = ('f4m', 'f4r')
    )

    switch = {
        'run': req_bot.run,
        'all': cli.display_all,
        'count': cli.display_count,
        'non_priority': cli.display_non_priority,
        'priority': cli.display_priority,
        'reset': cli.reset
    }

    for arg in argv:
        if arg in switch:
            switch[arg]()
            break
    

if __name__ == '__main__':
    main()
